function chkcomplex()
{
    var t1:HTMLInputElement=<HTMLInputElement> document.getElementById("t1");
    var t2:HTMLInputElement=<HTMLInputElement> document.getElementById("t2");
    var t3:HTMLInputElement=<HTMLInputElement> document.getElementById("t3");

    var data:string=t1.value;
    var real:number;
    var imaginary:number;

    var i: number = data.indexOf("+");
    if(i!= -1)
    {
        real= +data.substring(0,i);
        imaginary = +data.substring(i+1,data.length-1);
        t2.value="real :"+real;
        t3.value="imaginary :"+imaginary;
    }
    else
    {
        real= +data.substring(0,data.length);
        t2.value= "real:"+real;
        t3.value= "imaginary:0";
    }
}