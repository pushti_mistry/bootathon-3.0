var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function val() {
    if (t1.value.length > 0 && t2.value.length > 0) {
        alert("successfully performed");
    }
    else {
        alert("fill details in textbox");
    }
}
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function div() {
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();
}
function sin() {
    var a = Math.PI / 180 * parseFloat(t1.value);
    var b = Math.sin(a);
    t3.value = b.toString();
}
function cs() {
    var a = Math.PI / 180 * parseFloat(t1.value);
    var b = Math.cos(a);
    t3.value = b.toString();
}
function tan() {
    var a = Math.PI / 180 * parseFloat(t1.value);
    var b = Math.atan(a);
    t3.value = b.toString();
}
function srt() {
    var a = parseFloat(t1.value);
    var b = Math.sqrt(a);
    t3.value = b.toString();
}
function pwr() {
    var a = parseFloat(t1.value);
    var b = parseFloat(t2.value);
    var c = Math.pow(a, b);
    t3.value = c.toString();
}
//# sourceMappingURL=calculate.js.map