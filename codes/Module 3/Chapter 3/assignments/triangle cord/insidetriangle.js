function chkcords() {
    var t1 = document.getElementById("t11");
    var t2 = document.getElementById("t12");
    var t3 = document.getElementById("t21");
    var t4 = document.getElementById("t22");
    var t5 = document.getElementById("t31");
    var t6 = document.getElementById("t32");
    var p1 = document.getElementById("p1");
    var p2 = document.getElementById("p2");
    var x1 = parseFloat(t1.value);
    var y1 = parseFloat(t2.value);
    var x2 = parseFloat(t3.value);
    var y2 = parseFloat(t4.value);
    var x3 = parseFloat(t5.value);
    var y3 = parseFloat(t6.value);
    var x = parseFloat(p1.value);
    var y = parseFloat(p2.value);
    var abc = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
    var pab = Math.abs((x * (y1 - y2) + x1 * (y2 - y) + x2 * (y - y1)) / 2);
    var pbc = Math.abs((x * (y2 - y3) + x2 * (y3 - y1) + x3 * (y - y2)) / 2);
    var pac = Math.abs((x * (y3 - y1) + x2 * (y3 - y) + x3 * (y - y1)) / 2);
    var sum = pab + pbc + pac;
    if ((abc - sum) < 0.0000001) {
        document.getElementById("ans").innerHTML = "Points are inside the triangle";
    }
    else {
        document.getElementById("ans").innerHTML = "Points aren't  inside the triangle";
    }
}
