function chkcomplex() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    var t3 = document.getElementById("t3");
    var data = t1.value;
    var real;
    var imaginary;
    var i = data.indexOf("+");
    if (i != -1) {
        real = +data.substring(0, i);
        imaginary = +data.substring(i + 1, data.length - 1);
        t2.value = "real :" + real;
        t3.value = "imaginary :" + imaginary;
    }
    else {
        real = +data.substring(0, data.length);
        t2.value = "real:" + real;
        t3.value = "imaginary:0";
    }
}
